var utils = require('../module.utils');
var assert = require('assert');

describe('module.utils', function() {
    describe('#isEmpty', function() {
        it('should return true when dictionary is empty',
            function() {
                assert.equal(utils.isEmpty({}), true);
            }
        );

        it('should return true when dictionary is null',
            function() {
                assert.equal(utils.isEmpty(null), true);
            }
        );

        it('should return false with real dictionary',
            function() {
                assert.equal(utils.isEmpty({"one": "1"}), false);
            }
        );

        it('should throw exception when value is not a dictionary',
            function() {
                assert.ifError(utils.isEmpty("testing"));
            }
        );
    });


});