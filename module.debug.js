


var self = module.exports = {
    getMethods: (obj) => Object.getOwnPropertyNames(obj).filter(item => typeof obj[item] === 'function');

