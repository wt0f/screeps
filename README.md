Screeps Game Code
==================


Room Priorities
---------------

1. Defense
1. Healing
1. Building
1. Repairing
1. Harvesting
1. Upgrading


TODO
----

1. Priorities need to be a bit more dynamic. Problem is that upgrading becomes
   starved as all the workers are tasked with building and delivering energy
   to the spawn long before being tasked with upgrading. 
1. Need a happiness score to move energy between spawn, controller and
   construction projects. 
1. Birth control regulation. No need to spawn a lot of workers if there is
   not enough work for them. 
1. Use flags for construction of sites (energy storage first)
1. Auto build roads. Track workers to find most travelled routes.
1. Need to differentiate between deliver task and upgrade.
1. Need to scan for and repair construction sites that have degraded. At what
   level does the structure need to be repaired?
   

Worker State Diagram
--------------------

<pre>                                                                                                                         
                                                                                                                         
                                                  .─────────────.                                                        
      ┌─────────────┬───────────────────────────▶(  Unassigned   )◀───────────────────────────┬─────────────────────────┐
      │             │                             `─────────────'                             │                         │
      │             │                                    │                                    │                         │
      │             │                                    └───────┐                            │                         │
      │     ┌──────────────┐                                     ▼                    ┌───────────────┐                 │
      │     │No more energy│                    ╔═════════════════════════════════╗   │  Energy = 0?  │◀───────┐        │
      │     │  at source?  │                    ║locateClosestAvailEnergySource() ║   └───────────────┘        │        │
      │     └──────────────┘                    ╚═════════════════════════════════╝                            │        │
      │             ▲                                            │                                             │        │
      │             │                                            └─┐                                           │        │
      │             │                                              ▼            ╔══════════════════════════╗   │        │
      │             │  ╔═══════════════════════════════╗    .─────────────.  ┌──║locateClosestBuildTarget()║   │        │
      │             │  ║    locateRoomController()     ║ ┌▶(     Move      )◀┘  ╚══════════════════════════╝   │        │
      │           ┌─┘  ║  locateClosestBuildTarget()   ║─┘  `─────────────'                   ▲                │        │
      │           │    ║ locateClosestDeliveryTarget() ║           │                        ┌─┘                │        │
      │           │    ╚═══════════════════════════════╝           │                        │                  │        │
      │           │                    ▲                           │                 ┌─────────────┐           │        │
      │           │                    └───┐                     ┌─┘                 │ Build done? │           │        │
      │           │                        │                     │                   │ Energy > 0? │◀──────────┤        │
      │     .───────────.           ┌────────────┐               │                   └─────────────┘           │        │
      │    (  Recharge   )────┐     │ Energy at  │               ▼                                             │        │
      │     `───────────'     └────▶│ structure  │          ┌─────────┐     ┌───────────────────┐              │        │
      │           ▲                 │ capacity?  │          │  Goal   │     │ Mission = Build?  │        .───────────.  │
      │           └───┐             └────────────┘          │ Reached │────▶│Reached Structure? │──────▶(    Build    ) │
      │               │                                     └─────────┘     └───────────────────┘        `───────────'  │
      │  ┌────────────────────────┐                              │                                                      │
      │  │     Mission = * ?      │                              │                                                      │
      │  │ Reached Energy Source? │◀─────────────────────────────┤                                                      │
      │  └────────────────────────┘                              │                                                      │
      │                                                          │           ┌────────────────────┐                     │
      │                         ┌───────────────────┐            │           │ Mission = Upgrade? │                     │
      │                         │ Mission= Harvest? │            ├──────────▶│ Reached Structure? │                     │
      │  ┌──────────────┐       │Reached Structure? │◀───────────┘           └────────────────────┘                     │
      └──│ Energy = 0?  │       └───────────────────┘                                   │                               │
         └──────────────┘                 │                                             │                               │
                 ▲                        │                                             ▼                               │
                 └─┐                      │                                       .───────────.       ┌──────────────┐  │
                   │                      │                                      (   Upgrade   )─────▶│ Energy = 0?  │──┘
            .─────────────.               │                                       `───────────'       └──────────────┘   
           (   Discharge   )◀─────────────┘                                             │                                
            `─────────────'                          ╔═══════════════════════╗          │                                
                   │                                 ║locateRoomController() ║◀─┐       └───┐                            
                   │                                 ╚═══════════════════════╝  │           │                            
                   ▼                                             │              │           ▼                            
            ┌────────────┐                             ┌─────────┘              │  ┌─────────────────┐                   
            │ Structure  │                             │                        │  │ Not in range of │                   
            │   Full?    │                             ▼                        └──│   Controller?   │                   
            └────────────┘                        ○○○○○○○○○○○                      └─────────────────┘                   
                   │                             ○   Move    ○                                                           
                   └──┐                           ○○○○○○○○○○○                                                            
                      ▼                                ▲                                                                 
      ╔══════════════════════════════╗                 │                                                                 
      ║locateClosestDeliveryTarget() ║─────────────────┘                       ┌──────────────────────────────────────┐  
      ╚══════════════════════════════╝                                         │Legend                                │  
                                                                               │                                      │  
                                                                               │                                      │  
                                                                               │  .─────────.     ┌───────────────┐   │  
                                                                               │ (   Task    )    │   Condition   │   │  
                                                                               │  `─────────'     └───────────────┘   │  
                                                                               │           ╔══════════════╗           │  
                                                                               │           ║   Function   ║           │  
                                                                               │           ╚══════════════╝           │  
                                                                               └──────────────────────────────────────┘  </pre>