/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('module.priorityQueue');
 * mod.thing == 'a thing'; // true
 */
class PriorityQueue {
    constructor() {
        this.data = [];
    }
    push(priority, element = {}, rest = {}) {
        for (var i = 0; i < this.data.length && this.data[i]['priority'] < priority; i++);
        this.data.splice(i, 0, { priority: priority, data: element });
    }
    shift() {
        if (this.size() == 0)
            return undefined
        else
            return this.data.shift()['data'];
    }
    size() {
        return this.data.length;
    }
    length() {
        return this.data.length;
    }
    clear() {
        console.log("*** Clearing work queue ***");
        this.data = [];
    }
};

module.exports = PriorityQueue;