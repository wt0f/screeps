var utils = require('module.utils');
var roles = require('roles');

var self = module.exports = {

    create: (room) => {
        this.room = room;

        let newName = 'Worker' + Game.time;
        let res = null;

        // locate the spawn for the designated room
        let spawns = _.filter(Game.spawns, (spawn) => {
            return spawn.room.name == room.name;
        })
        // TODO: work out how to spawn when more than 1 spawn in room
        let spawn = spawns[0];

        // BODYPART_COST: {
        //     "move": 50,
        //     "work": 100,
        //     "attack": 80,
        //     "carry": 50,
        //     "heal": 250,
        //     "ranged_attack": 150,
        //     "tough": 10,
        //     "claim": 600
        // }
        if (room.energyAvailable >= 700) {
            res = spawn.spawnCreep([WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE],
                            newName,
                            {memory: {role: 'worker',
                                      mission: roles.M_unassigned,
                                      task: roles.T_unassigned}});
        } else if (room.energyAvailable >= 500) {
            res = spawn.spawnCreep([WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE],
                            newName,
                            {memory: {role: 'worker',
                                      mission: roles.M_unassigned,
                                      task: roles.T_unassigned}});
        } else if (room.energyAvailable >= 300) {
            res = spawn.spawnCreep([WORK,CARRY,CARRY,MOVE,MOVE],
                            newName,
                            {memroy: {role: 'worker',
                                      mission: roles.M_unassigned,
                                      task: roles.T_unassigned}});
        };
        return res;
    },

    /** @param {Creep} creep **/
    run: (creep) => {
        this.creep = creep;

        //console.log("roleHarvester.run()");

        // Initialize this creep if it has lost its memory
        if (!creep.memory.mission) {
            creep.memory.mission = roles.M_unassigned;
        }
        if (!creep.memory.task) {
            creep.memory.task = roles.T_unassigned;
        }
        var goal = Game.getObjectById(creep.memory.goal);
        var target = Game.getObjectById(creep.memory.target);

        //console.log("harvester task=", creep.memory.task);
        switch (creep.memory.task) {

            case roles.T_unassigned:
                // All workers must gather energy to do anything
                let source = utils.locateClosestAvailEnergySource(creep);
                if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    self.nextTask(creep, roles.T_move, {setGoal: source.id});
                }
                break;


            case roles.T_move:
                let pathColor = '#ffffff';
                switch(creep.memory.mission) {
                    case roles.M_build:
                        pathColor = '#f00000';
                        break;
                    case roles.M_harvest:
                        pathColor = '#00f000';
                        break;
                    case roles.M_upgrade:
                        pathColor = '#f0f000';
                        break;
                };

                let goal = Game.getObjectById(creep.memory.goal);
                var result = creep.moveTo(goal,
                                    {visualizePathStyle: {stroke: pathColor}});

                // Register using the sector as a road
                utils.update_road_plan(creep.pos);

                // console.log("res = ", res);
                switch(result) {
                    case OK:
                        var distance = creep.pos.getRangeTo(goal);
                        let sector = creep.room.lookAt(goal);
                        let allowedDistance = 1;
                        sector.forEach(function(def) {
                            if (def.type == LOOK_CONSTRUCTION_SITES) {
                                allowedDistance = 3;
                            }
                            if (def.type == LOOK_STRUCTURES) {
                                if (def.structure == STRUCTURE_CONTROLLER) {
                                    allowedDistance = 3;
                                }
                            }
                        });
                        //console.log("distance = ", distance);
                        if (distance <= allowedDistance) {
                            //console.log("reached goal, reassing to target");
                            // reached goal, move to next state
                            creep.memory.target = creep.memory.goal;
                            creep.memory.goal = null;
                            var target = Game.getObjectById(creep.memory.target);

                            var targets = creep.room.lookAt(target);
                            switch (targets[0].type) {
                                case LOOK_SOURCES:
                                    //console.log("found energy source");
                                    creep.memory.task = roles.T_recharge;
                                    creep.harvest(target);
                                    break;
                                case LOOK_STRUCTURES:
                                    //console.log("starting discharge");
                                    creep.memory.task = roles.T_discharge;
                                    creep.transfer(target, RESOURCE_ENERGY);
                                    break;
                                case LOOK_CONSTRUCTION_SITES:
                                    //console.log("found build structure");
                                    creep.memory.task = roles.T_build;
                                    creep.build(target);
                                    break;
                                default:
                                    //console.log("found nothing");
                                    //console.log(JSON.stringify(targets));
                                    break;
                            }
                        }
                        break;
                    case ERR_TIRED:
                    case ERR_BUSY:
                        break;
                    case ERR_NO_BODYPART:
                        // Creep can not move any longer. Kill it off.
                        creep.suicide();
                        break;
                    case ERR_INVALID_TARGET:
                        // Find a new target
                        var target = utils.locateClosestBuildTarget(creep);
                        if (target) {
                            self.nextTask(creep, roles.T_move, {setGoal: target.id});
                        } else {
                            creep.say("No Work");
                            self.nextTask(creep, roles.T_unassigned, {setGoal: null});
                        }
                        break;
                };
                break;

            case roles.T_recharge:
                if (creep.carry.energy < creep.carryCapacity) {
                    let res = creep.harvest(target);
                    if (res == ERR_NOT_ENOUGH_RESOURCES) {
                        self.nextTask(creep, roles.T_unassigned, {setGoal: null});
                    }
                } else {
                    // finished recharging
                    let target = null;
                    switch(creep.memory.mission) {
                        case roles.M_build:
                            target = utils.locateClosestBuildTarget(creep);
                            creep.say("🚧 build");
                            break;
                        case roles.M_harvest:
                            target = utils.locateDeliveryTarget(creep);
                            creep.say("deliver");
                            break;
                        case roles.M_upgrade:
                            target = utils.locateRoomController(creep);
                            creep.say("upgrade");
                            break;
                        case roles.M_unassigned:
                            self.nextTask(creep, roles.T_unassigned, {setGoal: null});
                            break;
                    }
                    if (target) {
                        console.log(creep, "mission ", creep.memory.mission, " target ", target);
                        self.nextTask(creep, roles.T_move, {setGoal: target.id});
                    } else {
                        console.log(creep, ' was on mission ',
                            creep.memory.mission + '/' + creep.memory.task,
                            ' but now unassigned');
                        self.assign(creep, {mission: roles.M_unassigned});
                    }
                }
                break;


            case roles.T_discharge:
                if (creep.carry.energy > 0) {
                    let res = creep.transfer(target, RESOURCE_ENERGY);
                    //console.log("transfering energy: ", res);
                    if (res == ERR_FULL) {
                        console.log(creep, "energy full, can not transfer");
                        creep.say("resource full");
                        let target = utils.locateDeliveryTarget(creep);
                        console.log(target, " localDeliveryTarget");
                        if (target) {
                            self.nextTask(creep, roles.T_move, {setGoal: target.id});
                        }
                    }
                } else {
                    //console.log("transfer complete");
                    // finished transfer of energy
                    self.nextTask(creep, roles.T_unassigned, {setGoal: null});
                }
                break;


            case roles.T_build:
                if (creep.carry.energy > 0) {
                    let res = creep.build(target);

                    switch (res) {
                        case OK:
                            break;
                        case ERR_NOT_IN_RANGE:
                        case ERR_INVALID_TARGET:
                            let nextTarget = utils.locateClosestBuildTarget(creep);
                            if (nextTarget) {
                                self.nextTask(creep, roles.T_move, {setGoal: nextTarget.id});
                            } else {
                                self.nextTask(creep, roles.T_unassigned, {setGoal: null});
                            }
                            break;
                        default:
                            console.log(creep, " unhandled build result", res);
                            break;
                    }
                } else {
                    // finished transfer of energy
                    self.nextTask(creep, roles.T_unassigned, {setGoal: null});
                    creep.say("🔄 harvest");
                }
                break;


                case roles.T_upgrade:
                if (creep.carry.energy > 0) {
                    let res = creep.upgradeController(creep.room.controller);
                    //console.log("transfering energy: ", res);
                    if (res == ERR_NOT_IN_RANGE) {
                        let target = locateRoomController(creep);
                        if (target) {
                            self.nextTask(creep, roles.T_move, {setGoal: target.id});
                            creep.say("reaquiring");
                        }
                    }
                } else {
                    //console.log("transfer complete");
                    // finished transfer of energy
                    task.nextTask(creep, roles.T_unassigned, {setGoal: null});
                    creep.say("🔄 harvest");
                }
                break;
        };
    },

    nextTask: (creep, newTask, options) => {
        creep.memory.task = newTask;
        if (options.setGoal) {
            creep.memory.target = creep.memory.goal;
            creep.memory.goal = options.setGoal;
        }

        if (options.setAcheivement)
            creep.memory.acheivement = options.setAcheivement;

        if (options.clearAcheivement)
            delete creep.memory.acheivement;

        if (newTask == roles.T_unassigned)
            self.assign(creep, roles.M_unassigned);
    },

    assign: (creep, work) => {
        creep.memory.mission = (' ' + work.mission).slice(1);
        if (work.target) {
            creep.memory.target = (' ' + work.target).slice(1);
        }
    }

};


