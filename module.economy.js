

const RATE_INTERVAL = 10;     // ticks per period

class Economy {
    constructor() {

        this.unemployment_rate = undefined;         // unassigned creeps
        this.growth_rate = undefined;               // rate upgrades occur
        this.birth_rate = undefined;                // how often creeps are born
        this.construction_rate = undefined;         // how much construction
        this.energy_depleation_rate = undefined;    // how fast energy is going
        this.energy_utilization_rate = undefined;   // efficiency of energy depleation

        this.needs = [];

        // initialize memory structures
        Memory['economy'] = {'_internal': {} };

    }

    start_room_update(roomName) {
        // Record what rooms get updated
        Memory['economy']['_internal']['rooms'].push(roomName);

    }

    finish_room_update(roomName) {

    }

    start_tick() {
        console.log('**** start_tick()');
        Memory['economy']['_internal']['rooms'] = [];

    }

    record_construction_sites(roomName, sites) {
        if (!('construction' in Memory.rooms[roomName])) {
            console.log("creating room construction stats");
            Memory.rooms[roomName]['construction'] = {};
        }
        Memory.rooms[roomName]['construction'][Game.time] = sites.length;
    }

    finish_tick() {
        if (Game.time % RATE_INTERVAL == 0) {
            console.log("**** Period over, calculating state of union");

            // Look to see if there are any new roads to construct
            
            // Calculate the construction_rate
            Memory['economy']['_internal']['rooms'].forEach((roomName) => {
                console.log("Calculating construction rate in room " + roomName);
                var sum = 0;
                var count = 0;
                for (var [tick,datum] in Memory.rooms[roomName]['construction']) {
                    sum +- datum;
                    count += 1;
                }
                this.construction_site = datum / count;
                Memory['economy']['construction_rate'] = this.construction_site;
                // reset running data
                Memory.rooms[roomName]['construction'] = {};
            });


        }

    }


};

module.exports = Economy;