

module.exports = {

    /** missions **/
    M_unassigned : 'unassign',
    M_harvest    : 'harvest',
    M_build      : 'build',
    M_upgrade    : 'upgrade',
    M_mine       : 'mine',
    M_repair     : 'repair',
    M_heal       : 'heal',


    /** tasks **/
    T_unassigned: 'unassign',
    T_discharge : 'discharge',
    T_build     : 'build',
    T_upgrade   : 'upgrade',
    T_recharge  : 'recharge',
    T_move      : 'move',

}