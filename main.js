var roleWorker = require('role.worker');
var utils = require('module.utils');
var flags = require('module.flags');
var queue = require('module.priorityQueue');
var roles = require('roles');
var economy = require('module.economy');

let eco = new economy();

module.exports.loop = () => {
    //console.log("*** Start of event loop ***");
    eco.start_tick();

    // Update activity on any spawns
    for (let spawn in Game.spawns) {
        if(Game.spawns[spawn].spawning) {
            var spawningCreep = Game.creeps[Game.spawns[spawn].spawning.name];
            Game.spawns[spawn].room.visual.text(
                '🛠️' + spawningCreep.memory.role,
                Game.spawns[spawn].pos.x + 1,
                Game.spawns[spawn].pos.y,
                {align: 'left', opacity: 0.5});
        }
    }

    // insure that we have the basic memory structures



    // Check the flag to
    for (let flag in utils.gatherFlags()) {
        console.log(flag, "Processing");
    }

    // Update work queues
    for (let roomName in Game.rooms) {
        eco.start_room_update(roomName);

        let room = Game.rooms[roomName];

        let workQ = new queue();

        // insure that we have the basic memory structures
        if (! room.memory['road_construction']) {
            room.memory['road_construction'] = {};
        }

        // Check for building
        var construction_sites = room.find(FIND_MY_CONSTRUCTION_SITES);
        eco.record_construction_sites(roomName, construction_sites);
        construction_sites.forEach((site) => {
            let data = {mission: roles.M_build, target: site.id};
            workQ.push(3, data);
        })

        // for (let site in sectors) {
        //     console.log("site = ", sectors[site].id);
        //     
        // }

        // Check for repairing

        // Check for harvesting
        // console.log("room energy capacity avail = ", room.energyCapacityAvailable);
        // console.log("room energy avail = ", room.energyAvailable);
        if (room.energyCapacityAvailable > room.energyAvailable) {
            workQ.push(5, {mission: roles.M_harvest});
        }

        // Check for upgrading
        workQ.push(6, {mission: roles.M_upgrade});

        room.memory.workQ = workQ;

        eco.finish_room_update(roomName);
    }

    // assign work to creeps
    utils.gatherUnassignedCreeps().forEach((creep) => {
        var room = creep.room;
        var task = room.memory.workQ.shift();
        console.log(creep, "trying to assign ", task.mission + "/" + task.task);
        if (task != undefined) 
            roleWorker.assign(creep, task);
    });

    // Process each creep
    for (let name in utils.gatherCreeps()) {
        let creep = Game.creeps[name];

        //console.log("Processing ", creep);
        roleWorker.run(creep);
    }


    // Look for outstanding work
    for (let name in Game.spawns) {
        let room = Game.spawns[name].room;

        console.log("availale work: ", room.memory.workQ.size(), 
            'unemployed creeps: ', utils.gatherUnassignedCreeps().length);
        //console.log("num of creeps = ", utils.gatherCreeps().length());
        // Spawn a worker if there is excess work or there are no workers
        if (room.memory.workQ.length() > 0 ||
            utils.isEmpty(utils.gatherCreeps())) {
            // we have excess work, spawn a worker

            switch(roleWorker.create(room)) {
                case OK:
                    console.log("- Creating worker in ", room.name);
                    break;
            }
        }

    }

    eco.finish_tick();
}