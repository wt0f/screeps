/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('module.work');
 * mod.thing == 'a thing'; // true
 */

var roles = require(__dirname+'/roles');


var self = module.exports = {

    locateClosestAvailEnergySource: (creep) => {
        let target = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE,
            {filter: (sector) => {return self.sectorAvailAround(sector)}})
        return target
    },


    locateClosestBuildTarget: (creep) => {
        let target = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES,
            {filter: (sector) => {
                    var progress = sector.progressTotal - sector.progress;
                    var population = creep.room.lookForAt(LOOK_CREEPS, sector).length;
                    return (progress != 0) && (population == 0);
                }
            });
        return target
    },

    locateRoomController: (creep) => {
        return creep.room.controller;
    },

    locateDeliveryTarget: (creep) => {
        let targets = creep.room.find(FIND_STRUCTURES, {
                        filter: (structure) => {
                                // console.log(structure, "energy=", structure.energy, "  capacity=", structure.energyCapacity);
                                return (self.testDeliverbility(structure));
                        }});
        if (targets) {
            return targets[Math.round(Math.random() * targets.length)];
        } else {
            return null;
        }
    },

    testDeliverbility: (sector) => {
        switch (sector.structureType) {
            case STRUCTURE_EXTENSION:
            case STRUCTURE_SPAWN:
                return (sector.energy < sector.energyCapacity);
                break;
            // case STRUCTURE_CONTROLLER:
            //     return true;
            //     break;
            default:
                // console.log("rejecting ", sector.structureType);
                return false;
                break;
        }
    },

    sectorAvailAround: (sector) => {
        // Build a 1 sector boundry around sector
        let perimeter = this.creep.room.lookAtArea(sector.pos.y-1,
                sector.pos.x-1, sector.pos.y+1, sector.pos.x+1, true);

        // Look at returned sectors around designated sector and
        // find locations where creeps can be that are not already
        // occupied by a creep. If at least one sector is not
        // occupied, then return true
        return perimeter.some((obj) => {
            return obj.type == LOOK_TERRAIN &&
                   (obj.terrain == 'swamp' || obj.terrain == 'plain') &&
                   this.creep.room.lookForAt(LOOK_CREEPS, obj.x, obj.y).length == 0;
        })
    },

    gatherCreeps: (room) => {
        // Only clear memory if there is memory for creeps
        if (Memory.creeps) {
            for (let name in Memory.creeps) {
                // Creeps that have died are nolonger listed in the game and
                // are ready for reaping
                if(!Game.creeps[name]) {
                    delete Memory.creeps[name];
                    console.log('Clearing non-existing creep memory:', name);
                }
            }
        }

        // Get a list of all creeps remaing in game
        if (room === undefined) {
            return Game.creeps;
        } else {
            return Game.creeps.filter((cr) => {
                cr.room == room;
            })
        }
    },

    gatherUnassignedCreeps: () => {
        return _.filter(Game.creeps, (creep) => {
            return creep.memory.task == roles.T_unassigned;
        });
    },

    gatherFlags: () => {
        return Game.flags;
    },


    update_road_plan: (pos) => {
        if (self.isEmpty(pos.lookFor(LOOK_STRUCTURES)))
            pos.createConstructionSite(STRUCTURE_ROAD);
    },

    isEmpty: (obj) => {
        if (typeof obj != "object") {
            throw new Error('NOOBJ');
        }

        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
}

